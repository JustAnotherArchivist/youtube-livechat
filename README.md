A method to grab the live chat from YouTube

* Requires qwarc and realpath from GNU coreutils 8.23+.
* Execute as `livechat VIDEOID` where `VIDEOID` is the 11-character video ID from YouTube.
* You can pass multiple video IDs at once as well: `livechat VIDEOID1 VIDEOID2 ...`. They get executed sequentially. (To capture multiple streams' chat simultaneously, run `livechat` multiple times instead!)
* All live chats (i.e. "top" and "all") are grabbed.
